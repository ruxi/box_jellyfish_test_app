#include "test_app.h"
#include <boost/filesystem.hpp>
#include <boost/optional.hpp>
#include <boost/regex.hpp>
#include "logging.h"

int main(int argc, char ** argv)
{
    BoxJellyfish1 gugu;
    gugu.print();
    
    box_jellyfish::logging::LoggingOptions logging_options;
    logging_options.is_enabled = true;
    logging_options.log_filename = "box-jellyfish-test-app";
    logging_options.log_path = "";

    auto& logger = box_jellyfish::logging::LoggingManager::get();
    logger.set_options(logging_options);

    //TODO: intercept log records with a sink, and test everything is logged as expected
    LOG_RECORD(error) << "Logging an error";

    
    boost::optional<int> gugu1;
    gugu1.emplace(1);

    boost::filesystem::path path("C:\\code\\others\\box-jellyfish\\ruxis_docs\\scratch\\in.txt");
    if (!boost::filesystem::exists(path))
    {
        std::cout << "exists ----" << std::endl;    
    }

    //GUGU - don't forget to save some regex work
    boost::regex r("(\\d+)-(\\d+)");
    boost::smatch match;
    if (boost::regex_match(std::string("have no idea what it should match"), match, r)) 
    {
        auto start = std::stoul(match[1]);
        auto end = std::stoul(match[2]);
    }
    return 0;
}
